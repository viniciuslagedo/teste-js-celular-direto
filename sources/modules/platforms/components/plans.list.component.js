(function(){
	'use strict';

	angular
		.module('app.platforms')
		.component('planList', {
			templateUrl: 'modules/platforms/templates/plans.list.html',
			controller: PlatformsListController,
			controllerAs: 'vm'
		});

	function PlatformsListController($state, $stateParams, PlansService) {
		var	vm = this;
		vm.SKU = $stateParams.platformSku;
		vm.plans = [];

		vm.$onInit = populatePlatforms;
		vm.goBack = goBack;

		function populatePlatforms () {
			PlansService.getPlans(vm.SKU)
			.then(function (response) {
				var data = response.data;
				vm.plans = data.planos;
			})
			.catch(function (response) {
				console.error(response);
			});
		}

		function goBack() {
			$state.go('platforms');
		}
	}

})();
