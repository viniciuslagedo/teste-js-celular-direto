(function(){
	'use strict';

	angular
		.module('app.platforms')
		.component('platformsDetail', {
			templateUrl: 'modules/platforms/templates/platforms.detail.html',
			controller: PlatformsDetailController,
			controllerAs: 'vm',
			bindings: {
				platform: '<'
			}
		});

	function PlatformsDetailController ($state, $rootScope) {
		var vm = this;

		vm.goToPlans = goToPlans;
		vm.$onInit = activate;

		function activate() {
			formatDescription();
		}

		function formatDescription() {
			if (vm.platform.descricao) {
				vm.platform.descricao = vm.platform.descricao.split('|').join('<br />');
			}
		}

		function goToPlans () {
			$rootScope.platform = vm.platform;
			$state.go('plans', {
				platformSku: vm.platform.sku
			});
		}
	}

})();
