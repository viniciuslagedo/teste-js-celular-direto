(function(){
	'use strict';

	angular
		.module('app.platforms')
		.component('platformList', {
			templateUrl: 'modules/platforms/templates/platforms.list.html',
			controller: PlatformsListController,
			controllerAs: 'vm'
		});

	function PlatformsListController(PlatformsService) {
		var vm = this;

		vm.platforms = [];

		vm.$onInit = populatePlatforms;

		function populatePlatforms () {
			return PlatformsService.getPlatforms()
			.then(function (response) {
				var data = response.data;
				vm.platforms = data.plataformas;
			})
			.catch(function (response) {
				console.error(response);
			});
		}
	}

})();
