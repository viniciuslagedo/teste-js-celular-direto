(function(){
	'use strict';

	angular
		.module('app.platforms')
		.component('contractForm', {
			templateUrl: 'modules/platforms/templates/contract.form.html',
			controller: ContractFormController,
			controllerAs: 'vm'
		});

	function ContractFormController ($state, $stateParams, $rootScope, PlansService, ContractService) {
		var vm = this;
		vm.contract = {};
		vm.plan = $rootScope.plan ? $rootScope.plan : {sku: $stateParams.planSku};
		vm.platform = $rootScope.platform ? $rootScope.platform : {sku: $stateParams.platformSku};

		vm.$onInit = activate;
		vm.goBack = goBack;
		vm.sendContract = sendContract;

		function activate() {
			populatePlan();
		}

		function populatePlan() {
			if (!vm.plan.franquia) {
				PlansService.findPlanBySku(vm.platform.sku, vm.plan.sku)
				.then(function (response) {
					vm.plan = response;
				})
				.catch(function (response) {
					console.log(response);
				});
			}
		}

		function goBack() {
			$state.go('plans', {
				platformSku: vm.platform.sku
			});
		}

		function sendContract(formValid) {
			if (formValid) {
				vm.contract.plan = vm.plan;
				ContractService.sendContract(vm.contract);
			}
		}
	}

})();
