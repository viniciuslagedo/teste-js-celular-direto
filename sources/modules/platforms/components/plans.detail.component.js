(function(){
	'use strict';

	angular
		.module('app.platforms')
		.component('plansDetail', {
			templateUrl: 'modules/platforms/templates/plans.detail.html',
			controller: PlatformsDetailController,
			controllerAs: 'vm',
			bindings: {
				plan: '<',
				readOnly: '<'
			}
		});

	function PlatformsDetailController ($rootScope, $state, $stateParams) {
		var vm = this;
		vm.platform = $rootScope.platform ? $rootScope.platform : {sku: $stateParams.platformSku};
		vm.readOnly = vm.readOnly ? vm.readOnly : false;

		vm.goToContract = goToContract;

		function goToContract() {
			$rootScope.plan = vm.plan;
			$state.go('contract',{
				platformSku: vm.platform.sku,
				planSku: vm.plan.sku
			});
		}
	}

})();
