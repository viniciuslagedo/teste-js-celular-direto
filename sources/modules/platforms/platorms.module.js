(function(){
	'use strict';

	angular
		.module('app.platforms', [
			'ui.router',
			'ui.utils.masks'
		]);
})();
