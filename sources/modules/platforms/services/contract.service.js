(function(){
	'use strict';

	angular
		.module('app.platforms')
		.service('ContractService', ContractService);

	ContractService.$inject = [];

	/////////

	function ContractService() {
		return {
			'sendContract': sendContract
		};

		function sendContract (contract) {
			console.log(contract);
		}
	}
})();
