(function(){
	'use strict';

	angular
		.module('app.platforms')
		.service('PlatformsService', PlatformsService);

	PlatformsService.$inject = ['$q', '$http', 'configFactory'];

	/////////

	function PlatformsService($q, $http, configFactory) {
		return {
			'getPlatforms': getPlatforms
		};

		function getPlatforms () {
			var deferred = $q.defer();

			$http.get(configFactory.api + '/plataformas')
			.then(function (response) {
				deferred.resolve(response);
			})
			.catch(function (response) {
				deferred.reject(response);
			});
			return deferred.promise;
		}
	}
})();
