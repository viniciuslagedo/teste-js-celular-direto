(function(){
	'use strict';

	angular
		.module('app.platforms')
		.service('PlansService', PlansService);

	PlansService.$inject = ['$q', '$http', 'configFactory'];

	/////////

	function PlansService($q, $http, configFactory) {
		return {
			'getPlans': getPlans,
			'findPlanBySku': findPlanBySku
		};

		function getPlans (platformSku) {
			var deferred = $q.defer();

			$http.get(configFactory.api + '/planos/' + platformSku)
			.then(function (response) {
				deferred.resolve(response);
			})
			.catch(function (response) {
				deferred.reject(response);
			});

			return deferred.promise;
		}

		function findPlanBySku(platformSku, planSku) {
			var deferred = $q.defer();

			getPlans(platformSku)
			.then(function (response) {
				var planos = response.data.planos;
				angular.forEach(planos, function(plan){
					if (plan.sku === planSku) {
						deferred.resolve(plan);
					}
				});
			})
			.catch(function (response) {
				deferred.reject(response);
			});

			return deferred.promise;

		}
	}
})();
