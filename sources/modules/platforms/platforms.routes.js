(function(){
	'use strict';

	angular
		.module('app.platforms')
		.config(routes);

	routes.$inject = ['$stateProvider'];

	function routes ($stateProvider, $urlRouterProvider) {
		$stateProvider
			.state('home', {
				url: '',
				template: '<platform-list></platform-list>'
			})

			.state('platforms', {
				url: '/',
				template: '<platform-list></platform-list>'
			})

			.state('plans', {
				url: '/:platformSku/plans',
				template: '<plan-list></plan-list>'
			})

			.state('contract', {
				url: '/:platformSku/plans/:planSku/contract',
				template: '<contract-form></contract-form>'
			})
		;

	}
})();
