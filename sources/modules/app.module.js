(function(){
	'use strict';

	angular
		.module('app', [
			// Módulos da Aplicação
			'app.core',
			'app.platforms'
		]);
})();
