(function(){
	'use strict';

	angular
		.module('app.core')
		.config(routes)
		.run(states);

	routes.$inject = ['$stateProvider', '$urlRouterProvider'];
	states.$inject = ['$rootScope', '$state', '$stateParams'];

	function routes ($stateProvider, $urlRouterProvider) {
		$urlRouterProvider.otherwise('404');

		$stateProvider
			.state('404', {
				url: '/404',
				controller: 'ErrorsController',
				controllerAs: 'vm',
				views: {
					main: {
						templateUrl: 'modules/core/views/404.html',
					}
				},
				page: {
					title: 'Token Expirado!',
					classes: {
						html: '404-html',
						body: '404-body'
					}
				}
			})
		;

	}

	function states ($rootScope, $state, $stateParams){
		$rootScope.$state = $state;
		$rootScope.$stateParams = $stateParams;
	}
})();
