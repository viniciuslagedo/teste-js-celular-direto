(function() {
	'use strict';

	angular
		.module('app.core')
		.constant('CONFIGLOCAL', {
			api: 'https://private-59658d-celulardireto2017.apiary-mock.com',
			appUrl: 'http://localhost',
			baseUrl: '/',
			debug: true
		})
		.constant('CONFIGPRD', {
			api: 'https://private-59658d-celulardireto2017.apiary-mock.com',
			appUrl: 'https://teste-js-celular-direto.herokuapp.com/',
			baseUrl: '/',
			debug: false
		});
})();
