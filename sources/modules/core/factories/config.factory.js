(function(){
	'use strict';

	angular
		.module('app.core')
		.factory('configFactory', configFactory);

	configFactory.$inject = ['CONFIGLOCAL', 'CONFIGPRD'];

	/////////

	function configFactory (CONFIGLOCAL, CONFIGPRD) {
		var hostname = window.location.hostname;
		var config;

		if(hostname.includes('localhost') || hostname.includes('127.0.0.1')) {
			config = CONFIGLOCAL;
		}
		else {
			config = CONFIGPRD;
		}

		if (!config) {
			throw new Error('Não foi possível detectar a url utilizada na aplicação');
		}

		return config;
	}
})();
