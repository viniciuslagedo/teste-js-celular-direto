(function(){
	'use strict';

	angular
		.module('app.core', [
			'ngAria',
			'ngCookies',
			'ngResource',
			'ngSanitize',
			'ngTouch',
			'ui.router'
		]);
})();
