(function(){
	'use strict';

	angular
		.module('app.core')
		.config(html5mode)
		.config(enableLogging)
		.config(httpInterceptor)
		.run(browserFix);

	html5mode.$inject				= ['$locationProvider'];
	enableLogging.$inject 	= ['$logProvider', 'configFactoryProvider'];
	httpInterceptor.$inject	= ['$httpProvider'];
	browserFix.$inject			= ['$browser'];

	/////////

	function html5mode($locationProvider) {
		// Remove a configuração de rotas no HTML5
		$locationProvider.html5Mode({
			enabled: false,
			requireBase: false
		});
		$locationProvider.hashPrefix('!');
	}

	function enableLogging ($logProvider, configFactoryProvider) {
		var config = configFactoryProvider.$get();

		$logProvider.debugEnabled(config.debug);
	}

	function httpInterceptor ($httpProvider) {
		$httpProvider.defaults.headers.put = {
			'Content-Type': 'application/json;charset=utf-8'
		};

		$httpProvider.defaults.headers.post = {
			'Content-Type': 'application/json;charset=utf-8'
		};

		$httpProvider.defaults.headers.patch = {
			'Content-Type': 'application/json;charset=utf-8'
		};
	}

	function browserFix ($browser) {
		$browser.baseHref = function () { return '/'; };
	}
})();
