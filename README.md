# Teste JS Celular Direto #

## Documentação ##

### Dependências ###

Para executar o projeto é necessário instalar as seguintes tecnologias:

- [Git > v2.10.x](https://git-scm.com/downloads)
- [Node.js > v7.7.x](https://nodejs.org/en/download/)
- [Yarn > v0.21.x](https://yarnpkg.com/latest.msi)
- [NPM > v4.0.x](https://github.com/npm/npm)

### Ambientes ###

Estes são os ambientes configurados: 

- **Localhost:**
	- [http://localhost:9000](http://localhost:9000)
- **Produção:**
	- [https://teste-js-celular-direto.herokuapp.com/](https://teste-js-celular-direto.herokuapp.com/)


### Instalação ###

Siga corretamente as instruções abaixo:

1. Instale o **Git**;

2. Instale o **Node**;
	- OBS.: O Node é necessário pois o projeto utiliza: 
		- **[Grunt](http://gruntjs.com/)** como gerenciador de tarefas.
		- **[Yarn](https://yarnpkg.com/)** e o **[Bower](http://bower.io/)** como gerenciadores de pacotes.

3. Instale o **Yarn**;
	- OBS.: Caso você está rodando no Windows, será necessário inserir os seguintes valores na variável de ambiente **%PATH%**:
		- **%LOCALAPPDATA%\Yarn\config\global\node_modules\\.bin**
		- **%LOCALAPPDATA%\Yarn\\.bin**

4. Feita as instalações, agora podemos clonar o repositório. No terminal, digite:
	- Caso utilize **[SSH](https://confluence.atlassian.com/bitbucket/set-up-ssh-for-git-728138079.html)**:
	
	```
	git clone git@bitbucket.org:viniciuslagedo/teste-js-celular-direto.git
	```

- Caso utilize **HTTPS**:
	
	```
	git clone https://[username]@bitbucket.org/viniciuslagedo/teste-js-celular-direto.git
	```

5. Abra o terminal, entre no diretório raíz do projeto e execute o seguinte comando que irá instalar todas as **dependências de desenvolvimento (yarn)** e **dependências de produção (bower)**:
	
	```
	yarn run setup
	```

Agora o projeto está pronto para ser executado. :)


### Utilização ###

- Para levantar o **servidor node** em modo de **desenvolvimento**, digite no terminal:
	
```
yarn run serve:dev
```

- Para levantar o **servidor node** com os arquivos de **produção**, digite no terminal:
	
```
yarn run serve:prd
```

- Para apenas **compilar** os arquivos para o ambiente de **staging**, digite no terminal:
	
```
yarn run build:stg
```

- Para apenas **compilar** os arquivos para o ambiente de **produção**, digite no terminal:
	
```
yarn run build:prd
```

- Para **preparar os arquivos para deploy no ambiente de staging**, digite no terminal:
	
```
yarn run deploy:stg
```

- Para **preparar os arquivos para deploy no ambiente de produção**, digite no terminal:
	
```
yarn run deploy:prd
```
***