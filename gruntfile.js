'use strict';

module.exports = function(grunt) {
	var path = require('path');
	var pkg = grunt.file.readJSON('package.json');
	var bwe = grunt.file.readJSON('bower.json');

	require('time-grunt')(grunt);

	require('load-grunt-config')(grunt, {
		configPath: path.join(process.cwd(), 'tasks'),
		data: {
			pkg: pkg,
			bwe: bwe,
			name: pkg.name.replace(/[^\w\s]/gi, ''),
			path: {
				root: './',
				src: 'sources',
				prd: 'artifacts',
				tests: 'tests',
				tasks: 'tasks',
				tmp: '.tmp',
				bower: 'libraries'
			}
		},

		jitGrunt: {
			staticMappings: {
				useminPrepare: 'grunt-usemin',
				ngtemplates: 'grunt-angular-templates',
				cachebreaker: 'grunt-cache-breaker',
				availabletasks: 'grunt-available-tasks',
				fileblocks: 'grunt-file-blocks'
			}
		}
	});

	grunt.registerTask('serve', 'Prepara as dependências e levanta o servidor web. Use ":development" ou ":production".', function(target) {
		if (target === 'development' || target === 'staging' || target === undefined) {
			return grunt.task.run([
				'build:staging',
				'browserSync:development',
				'watch'
			]);
		} else if (target === 'production') {
			return grunt.task.run([
				'build:production',
				'browserSync:production'
			]);
		}
	});

	grunt.registerTask('build', 'Compila todos os arquivos Javascript e CSS, além de copiar fontes, reduzir imagens e tratar o HTML. Use ":staging" ou ":production".', function(target) {
		if (target === 'staging') {
			return grunt.task.run([
				'clean:development',
				'wiredep',
				'concurrent:development',
				'postcss:development',
				'fileblocks',
				'copy:development'
			]);
		} else if (target === 'production') {
			return grunt.task.run([
				'clean:production',
				'wiredep',
				'useminPrepare',
				'concurrent:production',
				'postcss:production',
				'fileblocks',
				'copy:production',
				'ngtemplates',
				'concat',
				'ngAnnotate',
				'cssmin',
				'uglify',
				'filerev',
				'usemin',
				'cachebreaker',
				'htmlmin'
			]);
		}
	});

	grunt.registerTask('deploy', 'Prepara os arquivos para deploy, validando o Javascript e o CSS e gerando os artefatos. Use ":staging" ou ":production".', function(target) {
		if (target === 'staging') {
			return grunt.task.run([
				'newer:jshint',
				'newer:jscs',
				'build:staging',
				'file-creator:staging'
			]);
		} else if (target === 'production' || target === 'qa') {
			return grunt.task.run([
				'newer:jshint',
				'newer:jscs',
				'build:production',
				'file-creator:production',
			]);
		}
	});

	grunt.registerTask('help', ['availabletasks']);
};
