'use strict';

// Minifica o HTML
module.exports = {
	production: {
		options: {
			removeComments: true,
			collapseWhitespace: true,
			conservativeCollapse: false,
			collapseBooleanAttributes: true,
			removeCommentsFromCDATA: true,
			removeOptionalTags: true,
			minifyJS: true
		},
		files: [{
			expand: true,
			cwd: '<%= path.prd %>',
			src: ['*.html', 'modules/**/*.html'],
			dest: '<%= path.prd %>'
		}]
	}
};
