module.exports = {
	css: {
		reload: '<%= path.tmp %>/styles/**/*.css'
	},
	all: {
		reload: true
	}
};
