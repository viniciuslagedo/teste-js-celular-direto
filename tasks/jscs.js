'use strict';

// Make sure code styles are up to par and there are no obvious mistakes
module.exports = {
	options: {
		config: '.jscsrc'
	},
	all: {
		src: [
			'gruntfile.js',
			'<%= path.dev %>/modules/**/*.js'
		]
	}
};
