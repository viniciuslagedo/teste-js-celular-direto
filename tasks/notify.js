'use strict';

// Exibe notificações após a finalização de certas tarefas
module.exports = {
	build: {
		options: {
			title: 'Build finalizado!',
			message: 'Todas as tarefas de build foram finalizadas com sucesso.'
		}
	},
	wiredep: {
		options: {
			title: 'Wiredep',
			message: 'Novas dependências do bower instaladas com sucesso.'
		}
	},
	js: {
		options: {
			title: 'JS',
			message: 'Tarefas de validação do JS realizadas.'
		}
	},
	sass: {
		options: {
			title: 'Sass',
			message: 'Novas folhas de estilo geradas com sucesso.'
		}
	},
	gruntfile: {
		options: {
			title: 'Gruntfile',
			message: 'Tarefas de validação do JS realizadas.'
		}
	},
	html: {
		options: {
			title: 'HTML',
			message: 'Arquivos HTML atualizados.'
		}
	}
};
