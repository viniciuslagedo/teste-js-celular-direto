'use strict';

// Concatena e minifica o arquivos
module.exports = {
	html: ['<%= path.prd %>/**/*.html'],
	css: ['<%= path.prd %>/styles/**/*.css'],
	js: ['<%= path.prd %>/scripts/**/*.js'],
	json: ['<%= path.prd %>/resources/**/*.json'],
	options: {
		assetsDirs: [
			'<%= path.prd %>',
			'<%= path.prd %>/favicons',
			'<%= path.prd %>/images',
			'<%= path.prd %>/fonts',
			'<%= path.prd %>/styles',
			'<%= path.prd %>/resources'
		],
		patterns: {
			js: [
				[/(favicons\/[^''""]*\.(png|jpg|jpeg|gif|webp|svg))/g, 'Replacing references to favicons'],
				[/(images\/[^''""]*\.(png|jpg|jpeg|gif|webp|svg))/g, 'Replacing references to images'],
				[/(resources\/[^''""]*\.(json))/g, 'Replacing references to resources']
			]
		}
	}
};
