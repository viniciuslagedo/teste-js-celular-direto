'use strict';

// Adiciona os prefixos dos navegadores
module.exports = {
	options: {
		processors: [
			require('autoprefixer')({ browsers: ['last 2 versions'] })
		]
	},
	development: {
		options: {
			map: true
		},
		files: [{
			expand: true,
			cwd: '<%= path.tmp %>/styles/',
			src: '**/*.css',
			dest: '<%= path.tmp %>/styles/'
		}]
	},
	production: {
		files: [{
			expand: true,
			cwd: '<%= path.tmp %>/styles/',
			src: '**/*.css',
			dest: '<%= path.tmp %>/styles/'
		}]
	}
};
