'use strict';

// Adiciona timestamp na chamada dos arquivos
module.exports = {
	production: {
		options: {
			match: [
				'scripts.*.js',
				'vendors.*.js',
				'main.*.css',
				'vendors.*.css',
			],
			replacement: 'time',
			src: {
				path: [
					'<%= path.prd %>/scripts/scripts.*.js',
					'<%= path.prd %>/scripts/vendors.*.js',
					'<%= path.prd %>/styles/main.*.css',
					'<%= path.prd %>/styles/vendors.*.css'
				]
			}
		},
		files: {
			src: ['<%= path.prd %>/index.html']
		}
	}
};
