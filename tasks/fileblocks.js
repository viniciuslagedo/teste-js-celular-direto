'use strict';

// Inclui os arquivos personalizados automaticamente na master.
module.exports = {
	options: {
		//rebuild: true
	},

	vendors: {
		src: ['<%= path.src %>/index.html'],
		blocks: {
			'vendorsscripts': {}
		}
	},

	main: {
		src: ['<%= path.src %>/index.html'],
		blocks: {
			'appscripts': {
				cwd: '<%= path.src %>/modules',
				src: [
					'app.module.js',
					'app.config.js',
					'app.routes.js',
					'**/*.module.js',
					'**/*.config.js',
					'**/*.route.js',
					'**/*.js'
				],
				prefix: 'modules'
			},

			'appstyles': {
				cwd: '<%= path.tmp %>/styles',
				src: [
					'app.css'
				],
				prefix: 'styles'
			}
		}
	}
};
