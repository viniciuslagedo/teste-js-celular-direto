'use strict';

// Minifica as imagens
module.exports = {
	production: {
		files: [{
			expand: true,
			cwd: '<%= path.src %>/images',
			src: '**/*.{png,jpg,jpeg,gif}',
			dest: '<%= path.prd %>/images'
		}]
	}
};
