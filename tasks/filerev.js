'use strict';

// Renomeia os arquivos para evitar cache.
module.exports = {
	options: {
		algorithm: 'md5',
		length: 12,
      encoding: 'utf8'
	},
	production: {
		src: [
			'<%= path.prd %>/scripts/**/*.js',
			//'<%= path.prd %>/resources/**/*.json',
			'<%= path.prd %>/fonts/**/*',
			'<%= path.prd %>/styles/**/*.css',
			//'<%= path.prd %>/images/**/*.{png,jpg,jpeg,gif,webp,svg}'
		]
	}
};
