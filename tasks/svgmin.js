'use strict';

// Minifica SVGs
module.exports = {
	production: {
		files: [{
			expand: true,
			cwd: '<%= path.src %>/images',
			src: '**/*.svg',
			dest: '<%= path.prd %>/images'
		}]
	}
};
