'use strict';

// Insere automaticamente as dependências do Bower.
module.exports = {
	js: {
		src: ['<%= path.src %>/index.html'],
		exclude: ['requirejs'],
		ignorePath: /\.\.\//
	},

	sass: {
		src: ['<%= path.src %>/styles/{,*/}*.{scss,sass}']
	}
};
