'use strict';

// Fica observando os arquivos para recarregar as telas
module.exports = {
	options: {
		spawn: false
	},
	bower: {
		files: ['bower.json'],
		tasks: ['wiredep, bsReload:all', 'notify:wiredep']
	},
	gruntfile: {
		files: ['gruntfile.js', '<%= path.tasks %>/**/{,*/}*.js'],
		tasks: ['newer:jshint:all', 'bsReload:all', 'notify:gruntfile']
	},
	index: {
		files: ['<%= path.src %>/index.html'],
		tasks: ['wiredep', 'fileblocks']
	},
	html: {
		files: ['<%= path.src %>/**/{,*/}*.html'],
		tasks: ['bsReload:all', 'notify:html']
	},
	sass: {
		files: ['<%= path.src %>/styles/**/{,*/}*.{scss,sass}'],
		tasks: ['sass:app', 'postcss:development', 'bsReload:css', 'notify:sass']
	},
	js: {
		files: ['<%= path.src %>/modules/**/*.js'],
		tasks: ['newer:jshint:all', 'newer:jscs:all', 'bsReload:all', 'notify:js'],
	}
};
