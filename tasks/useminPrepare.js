'use strict';

// Prepara para a minificação
module.exports = {
	html: '<%= path.src %>/index.html',
	options: {
		dest: '<%= path.prd %>',
		flow: {
			html: {
				steps: {
					js: ['concat', 'uglifyjs'],
					css: ['cssmin']
				},
				post: {
					js: [{
						name: 'concat',
						createConfig: function (context) {
							context.options.generated.options = {
								sourceMap: true
							};
						}
					}, {
						name: 'uglify',
						createConfig: function (context, block) {
							context.options.generated.options = {
								sourceMap : true,
								sourceMapIn: '.tmp/concat/' + block.dest.replace('.js', '.js.map')
							};
						}
					}]
				}
			}
		}
	}
};
