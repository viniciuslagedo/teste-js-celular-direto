'use strict';

// Valida o javascript
module.exports = {
	options: {
		jshintrc: '.jshintrc',
		reporter: require('jshint-stylish')
	},
	all: {
		src: [
			'gruntfile.js',
			'<%= path.src %>/modules/**/{,*/}*.js'
		]
	}
};
