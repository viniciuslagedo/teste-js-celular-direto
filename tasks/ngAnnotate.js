'use strict';

// Corrigi a injeção de dependências do angular antes de minificar
module.exports = {
	production: {
		files: [{
			expand: true,
			cwd: '<%= path.tmp %>/concat/scripts',
			src: '*.js',
			dest: '<%= path.tmp %>/concat/scripts'
		}]
	}
};
