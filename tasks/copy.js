'use strict';

// Copia os arquivos para os diretórios corretos.
module.exports = {
	options: {},

	production: {
		files: [{
			expand: true,
			cwd: '<%= path.bower %>/font-awesome/fonts',
			src: ['*.{eot,svg,ttf,woff,woff2,otf}'],
			dest: '<%= path.prd %>/fonts',
		}, {
			expand: true,
			cwd: '<%= path.bower %>/intl-tel-input/build/img',
			src: ['*.*'],
			dest: '<%= path.prd %>/img',
		}, {
			expand: true,
			cwd: '<%= path.tmp %>/images',
			src: ['generated/*'],
			dest: '<%= path.prd %>/images',
		}, {
			expand: true,
			cwd: '<%= path.root %>/terminal',
			src: ['appveyor.before-deploy.ps1'],
			dest: '<%= path.prd %>/',
			rename: function(dest) {
				'use strict';
				return dest + '/before-deploy.ps1';
			}
		}, {
			expand: true,
			cwd: '<%= path.root %>/terminal',
			src: ['appveyor.deploy.ps1'],
			dest: '<%= path.prd %>/',
			rename: function(dest) {
				'use strict';
				return dest + '/deploy.ps1';
			}
		}, {
			expand: true,
			dot: true,
			cwd: '<%= path.src %>',
			src: [
				'*.{ico,png,txt}',
				'*.html',
				'*.json',
				'*.js',
				'*.config',
				'*.htaccess',
				'favicons/**/*.{png,jpg,jpeg,gif,webp,svg}',
				'fonts/**/*.{eot,svg,ttf,woff,woff2,otf}',
				'images/**/*.{webp}',
				'resources/**/*.json'
			],
			dest: '<%= path.prd %>'
		}]
	},
	development: {
		files: [{
			expand: true,
			cwd: '<%= path.bower %>/font-awesome/fonts',
			src: ['*.{eot,svg,ttf,woff,woff2,otf}'],
			dest: '<%= path.src %>/fonts',
		}, {
			expand: true,
			cwd: '<%= path.bower %>/intl-tel-input/build/img',
			src: ['*.*'],
			dest: '<%= path.src %>/img',
		}]
	}
};
