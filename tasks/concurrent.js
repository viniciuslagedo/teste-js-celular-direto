'use strict';

// Executa tarefas em paralelo para agilizar a execução das tarefas
module.exports = {
	development: [
		'sass'
	],
	production: [
		'sass',
		'imagemin',
		'svgmin'
	]
};
