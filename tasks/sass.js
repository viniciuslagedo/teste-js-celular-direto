'use strict';

// Compila o SASS
module.exports = {
	app: {
		options: {
			outputStyle: 'expanded',
			sourceMap: true,
			outFile: '<%= path.tmp %>/styles'
		},
		files: [{
			expand: true,
			cwd: '<%= path.src %>/styles',
			src: ['*.{scss,sass}'],
			dest: '<%= path.tmp %>/styles',
			ext: '.css'
		}]
	}
};
