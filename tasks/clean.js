'use strict';

// Apaga os diretórios
module.exports = {
	production: {
		files: [{
			dot: true,
			src: [
				'<%= path.tmp %>',
				'<%= path.prd %>/{,*/}*',
				'!<%= path.prd %>/.git{,*/}*'
			]
		}]
	},
	development: {
		files: [{
			dot: true,
			src: [
				'<%= path.tmp %>',
				'<%= path.src %>/styles/*.css',
				'<%= path.src %>/libraries',
				'<%= path.src %>/procfile',
				'<%= path.src %>/package.json',
				'<%= path.src %>/server.js',
				'<%= path.src %>/.gitignore',
				'!<%= path.prd %>/.git{,*/}*'
			]
		}]
	}
};
