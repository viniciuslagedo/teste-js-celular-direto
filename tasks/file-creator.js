'use strict';

var pkg = require('../package.json');

// Cria os arquivos necessários para os ambientes de staging e production
module.exports = {
	staging: {
		files: [{
			// Create Procfile required by Heroku
			file: 'sources/procfile',
			method: function(fs, fd, done) {
				fs.writeSync(fd, 'web: node server.js');
				done();
			}
		}, {
			//Create package.json for Heroku for adding dependencies (ExpressJS)
			file: 'sources/package.json',
			method: function(fs, fd, done) {
				fs.writeSync(fd, '{\n');
				fs.writeSync(fd, '  "name": ' + (pkg.name ? '"' + pkg.name + '"' : '""') + ',\n');
				fs.writeSync(fd, '  "version": ' + (pkg.version ? '"' + pkg.version + '"' : '""') + ',\n');
				fs.writeSync(fd, '  "description": ' + (pkg.description ? '"' + pkg.description + '"' : '""') + ',\n');
				fs.writeSync(fd, '  "private": ' + (pkg.private ? '"' + pkg.private + '"' : '""') + ',\n');
				fs.writeSync(fd, '  "author": ' + (pkg.author ? JSON.stringify(pkg.author) : '"{}"') + ',\n');
				fs.writeSync(fd, '  "license": ' + (pkg.license ? '"' + pkg.license + '"' : '""') + ',\n');
				fs.writeSync(fd, '  "homepage": ' + (pkg.homepage ? '"' + pkg.homepage + '"' : '""') + ',\n');
				fs.writeSync(fd, '  "main": "server.js",\n');
				fs.writeSync(fd, '  "engines": ' + (pkg.engines ? JSON.stringify(pkg.engines) : '"{}"') + ',\n');
				fs.writeSync(fd, '  "dependencies": {\n');
				fs.writeSync(fd, '    "express": "^4.15.2",\n');
				fs.writeSync(fd, '    "gzippo": "^0.2.0"\n');
				fs.writeSync(fd, '  },\n');
				fs.writeSync(fd, '  "scripts": {\n');
				fs.writeSync(fd, '    "start": "node server.js"\n');
				fs.writeSync(fd, '  }\n');
				fs.writeSync(fd, '}');
				done();
			}
		}, {
			//Create server.js used by ExpressJS within Heroku
			file: 'sources/server.js',
			method: function(fs, fd, done) {
				fs.writeSync(fd, '"use strict";\n');
				fs.writeSync(fd, '\n');
				fs.writeSync(fd, 'var express 	= require("express");\n');
				fs.writeSync(fd, '\n');
				fs.writeSync(fd, 'var app 		= express();\n');
				fs.writeSync(fd, 'var port 		= process.env.PORT || 5000;\n');
				fs.writeSync(fd, '\n');
				fs.writeSync(fd, 'app.set("port", port);\n');
				fs.writeSync(fd, 'app.use(express.static(__dirname));\n');
				fs.writeSync(fd, 'app.use("/libraries", express.static(__dirname + "/libraries"));\n');
				fs.writeSync(fd, 'app.use("/styles", express.static(__dirname + "/.tmp/styles"));\n');
				fs.writeSync(fd, 'app.set("modules", __dirname + "modules");\n');
				fs.writeSync(fd, '\n');
				fs.writeSync(fd, 'app.get("/", function(req, res){\n');
				fs.writeSync(fd, '	res.sendfile("index");\n');
				fs.writeSync(fd, '});\n');
				fs.writeSync(fd, '\n');
				fs.writeSync(fd, 'app.listen(port, function(){\n');
				fs.writeSync(fd, '	console.log("Node app is running on port", port);\n');
				fs.writeSync(fd, '});\n');
				done();
			}
		}, {
			//Add .gitignore to ensure node_modules folder doesn't get uploaded
			file: 'sources/.gitignore',
			method: function(fs, fd, done) {
				fs.writeSync(fd, 'node_modules');
				done();
			}
		}]
	},

	production: {
		files: [{
			// Create Procfile required by Heroku
			file: 'artifacts/procfile',
			method: function(fs, fd, done) {
				fs.writeSync(fd, 'web: node server.js');
				done();
			}
		}, {
			//Create package.json for Heroku for adding dependencies (ExpressJS)
			file: 'artifacts/package.json',
			method: function(fs, fd, done) {
				fs.writeSync(fd, '{\n');
				fs.writeSync(fd, '  "name": ' + (pkg.name ? '"' + pkg.name + '"' : '""') + ',\n');
				fs.writeSync(fd, '  "version": ' + (pkg.version ? '"' + pkg.version + '"' : '""') + ',\n');
				fs.writeSync(fd, '  "description": ' + (pkg.description ? '"' + pkg.description + '"' : '""') + ',\n');
				fs.writeSync(fd, '  "private": ' + (pkg.private ? '"' + pkg.private + '"' : '""') + ',\n');
				fs.writeSync(fd, '  "author": ' + (pkg.author ? JSON.stringify(pkg.author) : '"{}"') + ',\n');
				fs.writeSync(fd, '  "license": ' + (pkg.license ? '"' + pkg.license + '"' : '""') + ',\n');
				fs.writeSync(fd, '  "homepage": ' + (pkg.homepage ? '"' + pkg.homepage + '"' : '""') + ',\n');
				fs.writeSync(fd, '  "main": "server.js",\n');
				fs.writeSync(fd, '  "engines": ' + (pkg.engines ? JSON.stringify(pkg.engines) : '"{}"') + ',\n');
				fs.writeSync(fd, '  "dependencies": {\n');
				fs.writeSync(fd, '    "express": "^4.15.2",\n');
				fs.writeSync(fd, '    "gzippo": "^0.2.0"\n');
				fs.writeSync(fd, '  },\n');
				fs.writeSync(fd, '  "scripts": {\n');
				fs.writeSync(fd, '    "start": "node server.js"\n');
				fs.writeSync(fd, '  }\n');
				fs.writeSync(fd, '}');
				done();
			}
		}, {
			//Create server.js used by ExpressJS within Heroku
			file: 'artifacts/server.js',
			method: function(fs, fd, done) {
				fs.writeSync(fd, '"use strict";\n');
				fs.writeSync(fd, '\n');
				fs.writeSync(fd, 'var express 	= require("express");\n');
				fs.writeSync(fd, 'var http 		= require("http");\n');
				fs.writeSync(fd, 'var gzippo 		= require("gzippo");\n');
				fs.writeSync(fd, '\n');
				fs.writeSync(fd, 'var app 		= express();\n');
				fs.writeSync(fd, 'var port 		= process.env.PORT || 5000;\n');
				fs.writeSync(fd, 'var server;\n');
				fs.writeSync(fd, '\n');
				fs.writeSync(fd, 'app.set("port", port);\n');
				fs.writeSync(fd, 'app.use(express.static(__dirname));\n');
				fs.writeSync(fd, 'app.use(gzippo.staticGzip("" + __dirname));\n');
				fs.writeSync(fd, '\n');
				fs.writeSync(fd, 'app.use("/*", function(req, res){\n');
				fs.writeSync(fd, '	res.sendfile("/index.html");\n');
				fs.writeSync(fd, '});\n');
				fs.writeSync(fd, '\n');
				fs.writeSync(fd, 'server = http.createServer(app);\n');
				fs.writeSync(fd, 'server.listen(port);\n');
				done();
			}
		}, {
			//Add .gitignore to ensure node_modules folder doesn't get uploaded
			file: 'artifacts/.gitignore',
			method: function(fs, fd, done) {
				fs.writeSync(fd, 'node_modules');
				done();
			}
		}]
	}
};
