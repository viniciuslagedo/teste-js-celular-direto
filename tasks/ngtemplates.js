'use strict';

// Run some tasks in parallel to speed up the build process
module.exports = {
	production: {
		options: {
			module: '<%= bwe.module %>',
			htmlmin: '<%= htmlmin.production.options %>',
			usemin: 'scripts/app.js'
		},
		cwd: '<%= path.src %>',
		src: 'modules/**/*.html',
		dest: '<%= path.tmp %>/templates.js'
	}
};
