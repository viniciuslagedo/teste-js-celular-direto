// Levanta o servidor
module.exports = {
	development: {
		bsFiles: [
			'<%= path.src %>/**/*.html',
			'<%= path.src %>/favicons/**/*.{png,jpg,jpeg,gif,webp,svg}',
			'<%= path.src %>/fonts/**/*.{eot,svg,ttf,woff,woff2}',
			'<%= path.src %>/images/**/*.{png,jpg,jpeg,gif,webp,svg}',
			'<%= path.src %>/modules/**/*.js',
			'<%= path.src %>/resources/**/*.json',
			'<%= path.tmp %>/styles/**/*.css'
		],
		options: {
			port: 9000,

			//host: 'dev-checkoutredirect.mundipagg.com',

			// proxy: {
			// 	target: 'dev-checkoutredirect.mundipagg.com:9000',
			// 	ws: true
			// },

			// https: true,

			browser: 'chrome',

			server: {
				baseDir: '<%= path.src %>',
				routes: {
					'/libraries': '<%= path.bower %>',
					'/styles': '<%= path.tmp %>/styles'
				}
			},

			ui: {
				port: 9001,
				weinre: {
					port: 9002
				}
			},

			notify: true,
			background: true,

			open: 'ui',
			debugInfo: true,

			logLevel:'debug',
			logPrefix: '<%= pkg.name %>',
			logConnections: true,
			logFileChanges: true,

			// tunnel: '<%= name %>',
			
			online: true,

			timestamps: false,

			watchTask: true,

			// Sincronizando os eventos entre os dispositívos
			ghostMode: {
				scroll: true,
				links: true,
				forms: true
			}
		}
	},

	production: {
		options: {
			port: 9000,

			//host: 'dev-checkoutredirect.mundipagg.com',

			// proxy: {
			// 	target: 'dev-checkoutredirect.mundipagg.com:9000',
			// 	ws: true
			// },

			// https: true,

			server: {
				baseDir: '<%= path.prd %>'
			},

			ui: {
				port: 9001,
				weinre: {
					port: 9002
				}
			},

			notify: true,
			background: false,

			open: 'local',
			debugInfo: false,

			logLevel:'info',
			logPrefix: '<%= pkg.name %>',
			logConnections: false,
			logFileChanges: false,

			tunnel: '<%= name %>',
			online: true,

			timestamps: false,

			watchTask: false,

			// Sincronizando os eventos entre os dispositívos
			ghostMode: {
				scroll: true,
				links: true,
				forms: true
			}
		}
	}
};
